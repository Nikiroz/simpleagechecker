(function ($) {
    $(function () {
    var w = window.innerWidth;
    var m = 768 ;
        $(document).ready(function () {
            if (!$.cookie('accept')) {
                var html = '';
                html += '<div class="backPopup" id="agePopup">';
                html += '<div class="qestionContent yesContent">';
                html += '<h2>Подтвердите свой возраст</h2>';
                html += '<p>Некоторые разделы нашего сайта не предназначены для лиц младше <span>18 лет.</span> Пожалуйста подтвердите свой возраст.</p>';
                html += '<div id="Yes" class="acceptButton" onselectstart="return false" onmousedown="return false">Да, мне есть 18</div><div id="No" class="acceptButton" onselectstart="return false" onmousedown="return false">Нет, мне нет 18</div>';
                html += '</div>';
                html += '<div class="qestionContent noContent">';
                html += '<h2>Доступ запрещен!</h2>';
                html += '<p>Извини, но ты еще не достаточно взрослый что-бы пользоваться нашим сайтом. Не растраивайся, посмотри лучше мультик, и приходи к нам когда подрастешь.</p>';
                html += '<iframe id="mult" width="'+$("#mult").width($("#mult").parent().width())+'" height="auto" src="https://www.youtube.com/embed/JRy73FUpE4Q" frameborder="0" allowfullscreen></iframe>';
                html += '</div></div>';

                $('body').append(html);

                $("#agePopup").show();

                $('.backPopup').animate({
                    opacity: 1
                }, 500, function () {
                    $('.backPopup').css({opacity: 1})
                });
                var accepYes = document.getElementById("Yes");
                accepYes.onclick = function (e) {
                    $("#agePopup").hide();
                    $.cookie('accept', true, {expires: 365, path: '/'});
                }
                var accepNo = document.getElementById("No");
                accepNo.onclick = function (e) {
                    $(".yesContent").hide();
                   if (w>m){ $('.qestionContent').animate({
                        width: 450
                    }, 500, function () {
                        $('#mult').show()
                    })}else{$('#mult').show()}
                    $(".noContent").show();
                }
            }
        });
    })
})(jQuery)
